package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.FeedbackTDao;
import com.jshoperxms.entity.FeedbackT;

@Repository("feedbackTDao")
public class FeedbackTDaoImpl extends BaseTDaoImpl<FeedbackT> implements FeedbackTDao {
	
	private static final Logger log = LoggerFactory.getLogger(DeliverAddressTDaoImpl.class);
}
