package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the member_location_t database table.
 * 
 */
@Entity
@Table(name="member_location_t")
@NamedQuery(name="MemberLocationT.findAll", query="SELECT m FROM MemberLocationT m")
public class MemberLocationT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String lid;

	private String address;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	private double lat;

	private double lng;

	private String memberid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;

	private int versiont;

	public MemberLocationT() {
	}

	public String getLid() {
		return this.lid;
	}

	public void setLid(String lid) {
		this.lid = lid;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public double getLat() {
		return this.lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return this.lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getMemberid() {
		return this.memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}

	public Date getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public int getVersiont() {
		return this.versiont;
	}

	public void setVersiont(int versiont) {
		this.versiont = versiont;
	}

}