package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.TemplatesetT;
import com.jshoperxms.service.TemplatesetTService;
@Service("templatesetTService")
@Scope("prototype")
public class TemplatesetTServiceImpl extends BaseTServiceImpl<TemplatesetT>implements TemplatesetTService {

	
	
}
