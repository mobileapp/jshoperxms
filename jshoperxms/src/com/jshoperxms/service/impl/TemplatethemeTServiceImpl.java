package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.TemplatethemeT;
import com.jshoperxms.service.TemplatethemeTService;

@Service("templatethemeTService")
@Scope("prototype")
public class TemplatethemeTServiceImpl extends BaseTServiceImpl<TemplatethemeT> implements TemplatethemeTService {
	
}
