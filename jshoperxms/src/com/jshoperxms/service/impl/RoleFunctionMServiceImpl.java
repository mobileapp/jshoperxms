package com.jshoperxms.service.impl;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.dao.RoleFunctionMDao;
import com.jshoperxms.entity.RoleFunctionM;
import com.jshoperxms.entity.RoleM;
import com.jshoperxms.service.RoleFunctionMService;
@Service("roleFunctionMService")
@Scope("prototype")
public class RoleFunctionMServiceImpl extends BaseTServiceImpl<RoleFunctionM>implements RoleFunctionMService {

	private Serial serial;

	private RoleFunctionMDao roleFunctionMDao;
	
	public RoleFunctionMDao getRoleFunctionMDao() {
		return roleFunctionMDao;
	}
	public void setRoleFunctionMDao(RoleFunctionMDao roleFunctionMDao) {
		this.roleFunctionMDao = roleFunctionMDao;
	}
	public Serial getSerial() {
		return serial;
	}
	public void setSerial(Serial serial) {
		this.serial = serial;
	}
	
	/**
	 * 增加角色权限
	 * @param rm
	 */
	@Transactional
	public void addRoleFunctionM(RoleM rm,String functionids){
		if(StringUtils.isNotBlank(functionids)){
			String []strs=StringUtils.split(functionids,StaticKey.SPLITDOT);
			for(String functionid :strs){
				RoleFunctionM rfm=new RoleFunctionM();
				rfm.setId(this.getSerial().Serialid(Serial.ROLEFUNCTION));
				rfm.setRoleid(rm.getId());
				rfm.setFunctionid(functionid);
				this.getRoleFunctionMDao().save(rfm);
			}
		}
	}



	
}
