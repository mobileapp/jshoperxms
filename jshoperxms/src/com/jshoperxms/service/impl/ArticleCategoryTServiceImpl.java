package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.ArticleCategoryT;
import com.jshoperxms.service.ArticleCategoryTService;

@Service("articleCategoryTService")
@Scope("prototype")
public class ArticleCategoryTServiceImpl extends BaseTServiceImpl<ArticleCategoryT>implements ArticleCategoryTService {

}
