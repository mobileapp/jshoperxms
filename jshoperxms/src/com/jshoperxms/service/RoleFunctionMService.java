package com.jshoperxms.service;

import com.jshoperxms.entity.RoleFunctionM;
import com.jshoperxms.entity.RoleM;

public interface RoleFunctionMService extends BaseTService<RoleFunctionM> {

	
	public void addRoleFunctionM(RoleM rm,String functionids);
}
