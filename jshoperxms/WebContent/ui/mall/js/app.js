define([ 
         'angular',
         'angular-route',
         'angular-resource',
         'jquery',
         'bootstrap3',
         'ngfileupload',
         './interceptors',
         './services'], function(angular) {
	'use strict';
	var app=angular.module('app', [ 'ngRoute','ngResource','myservices','myinterceptors','brandmodule']);
	return app;
});
