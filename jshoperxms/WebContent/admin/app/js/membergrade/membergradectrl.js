define(['./module'], function(membergrademodule) {
    'use strict';

    /*=====Begin Of Save and Edit MemberGradeT=====*/
    membergrademodule.controller('membergrade', ServerSaveMemberGrade);
    function ServerSaveMemberGrade($scope, $http, $location) {
        $scope.gtparams = [];
        $scope.index = 1;
        //通过location中的operate参数区分操作行为
        var operate = $location.search().operate;
        if (operate != undefined && operate == "save") {
            //控制保存按钮显示
            $scope.savebtn = {
                show: true
            };
            $scope.title = '添加会员等级制度信息&参数';
            $scope.status = "1";
            $scope.type = "1";
            $scope.mpstate = "1";
            //保存数据方法
            $scope.save = function() {
            	var type = $scope.type;
            	if (type == undefined || type == "") {
                    $scope.errors = {
                        show: true,
                        msg: "空"
                    }
                    return false;
                }
                var name = $scope.name;
                var start = $scope.start;
                var end = $scope.end;
                var increment = $scope.increment;
                var multiplypower = $scope.multiplypower;
                var mpstate = $scope.mpstate;
                var mpchangepower = $scope.mpchangepower;
                var discount = $scope.discount;
                var status = $scope.status;
                $http({
                    method: 'POST',
                    url: '../mall/member/membergrade/save.action',
                    data: {
                        'type':type,
                        'name':name,
                        'levelrangestart':start,
                        'levelrangeend':end,
                        'increment':increment,
                        'multiplypower':multiplypower,
                        'mpstate':mpstate,
                        'mpchangepower':mpchangepower,
                        'discount':discount,
                        'status':status
                    }
                }).success(function(data, status, headers, config) {
                    if (data.sucflag) {
                        $scope.info = {
                            show: true,
                            msg: '会员等级制度信息添加成功'
                        }
                        $scope.errors = {
                            show: false
                        }
                    }
                }).error(function(data, status, headers, config) {
                    $scope.errors = {
                        show: true,
                        msg: '系统异常'
                    }
                });

            }
        }

        //如果operate是edit则执行保存行为
        if (operate == 'edit') {
            var id = $location.search().id;
            if (id != undefined && id != "") {
                $http({
                    method: 'POST',
                    url: '../mall/member/membergrade/find.action',
                    data: {
                        'memberGradeId': id
                    }
                }).success(function(data, status, headers, config) {
                    if (data.sucflag) {
                        $scope.title = '正在会员等级制度进行编辑';
                        $scope.name = data.bean.name;
                        $scope.type = data.bean.type;
                        $scope.start = data.bean.start;
                        $scope.end = data.bean.end;
                        $scope.increment = data.bean.increment;
                        $scope.multiplypower = data.bean.multiplypower;
                        $scope.mpstate = data.bean.mpstate;
                        $scope.mpchangepower = data.bean.mpchangepower;
                        $scope.discount = data.bean.discount;
                        $scope.updatebtn = {
                            show: true
                        };
                    }
                }).error(function(data, status, headers, config) {
                    $scope.errors = {
                        show: true,
                        msg: '系统异常'
                    }
                });
                //更新数据方法
                $scope.update = function() {
                    var type = $scope.type;
                    var name = $scope.name;
                    var start = $scope.start;
                    var end = $scope.end;
                    var increment = $scope.increment;
                    var multiplypower = $scope.multiplypower;
                    var mpstate = $scope.mpstate;
                    var mpchangepower = $scope.mpchangepower;
                    var discount = $scope.discount;
                    var status = $scope.status;
                    $http({
                        method: 'POST',
                        url: '../mall/member/membergrade/update.action',
                        data: {
                            'memberGradeId':id,
                            'type':type,
                            'name':name,
                            'levelrangestart':start,
                            'levelrangeend':end,
                            'increment':increment,
                            'multiplypower':multiplypower,
                            'mpstate':mpstate,
                            'mpchangepower':mpchangepower,
                            'discount':discount,
                            'status':status
                        }
                    }).success(function(data, status, headers, config) {
                        if (data.sucflag) {
                            $scope.info = {
                                show: true,
                                msg: '会员等级制度信息更新成功'
                            }
                        }
                    }).error(function(data, status, headers, config) {
                        $scope.errors = {
                            show: true,
                            msg: '系统异常'
                        }
                    });
                }
            }
        }

        //表单字段验证
        function validate() {
            // 分组名称
            var name = $scope.name;
            if (name == undefined || name == "") {
                $scope.errors = {
                    show: true,
                    msg: "请输会员分组名称"
                }
                return false;
            }
            return true;
        }
    }
    /*=====End Of Save and Edit MemberGradeT=====*/

    /*=====Begin Of Find List MemberGradeT=====*/
    membergrademodule.controller('membergradelist', ServerMemberGradeListCtrl);
    //查询列表数据
    function ServerMemberGradeListCtrl($http,$location,$compile,$scope,$rootScope,$resource,DTOptionsBuilder,DTColumnBuilder,DTAjaxRenderer){
		var vm=this;
		vm.message='';
		vm.someClickHandler = someClickHandler;
		vm.selected={};
		vm.selectAll=false;
		vm.toggleAll=toggleAll;
		vm.toggleOne=toggleOne;
		vm.dtInstance={};
		var titleHtml='<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll,showCase.selected)">';
		vm.dtOptions=DTOptionsBuilder.newOptions().withOption('ajax',{
			type:'POST',
			url:'../mall/member/membergrade/findByPage.action',
			dataSrc:'data'
		})
		.withOption('processing',true)
		.withOption('paging',true)
		.withOption('serverSide',true)
		.withOption('createdRow',function(row,data,dataIndex){
			$compile(angular.element(row).contents())($scope);
		})
		.withOption('headerCallback', function(header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('stateSave', true)
        .withOption('rowCallback',rowCallback)
		.withPaginationType('full_numbers')
		.withLanguageSource('./app/language/chinese.json')
		
		$scope.$on('handleRequest',function(){
			
		});
		$rootScope.getTableData=function serverData(){
			var req;
		}
		
		vm.dtColumns=[
		              DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable().renderWith(function(data,type,full,meta){
		            	  vm.selected[full.id]=false;
		            	  return '<input type="checkbox" ng-model="showCase.selected['+data.id+']" ng-click="showCase.toggleOne(showCase.selected)">';
		              }),
		              DTColumnBuilder.newColumn('id').withTitle('ID').notVisible(), 
		      		  DTColumnBuilder.newColumn('name').withTitle('分组名称').notSortable(), 
		      		  DTColumnBuilder.newColumn('type').withTitle('类型').notSortable(), 
		      		  DTColumnBuilder.newColumn('creatorid').withTitle('创建人').notSortable(), 
		      		  DTColumnBuilder.newColumn('updatetime').withTitle('更新时间').notSortable(), 
		      		  DTColumnBuilder.newColumn('status').withTitle('状态').notSortable(), 
		      		  DTColumnBuilder.newColumn('versiont').withTitle('版本号').notSortable(), 
		      		  DTColumnBuilder.newColumn(null).withTitle('操作').notSortable().renderWith(actionHtml)];
		function actionHtml(data,type,full,meta){
			return '<button class="btn btn-warning" ng-click="edit('+data.id+')"><i class="fa fa-edit"></i></button>';
		}
		//表格中编辑按钮
		$scope.edit=function(id,name){
			$location.path('/membergrade').search({'operate':'edit','id':id});
		}
		
		/**
		 * 跳转到添加商品类型和参数页面
		 */
		$scope.save=function(){
			$location.path('/membergrade').search({'operate':'save'});
		}
		
		$scope.del=function(){
			var i=0;
			var ids=[];
			angular.forEach(vm.selected, function(data,index,array){
				if(data){
					i++;
					ids.push(index);
				}
			});
			if(i==0){
				$scope.errors={
						show:true,
						msg:'请选择一条记录'
				}
			}else{
				$scope.errors={
						show:false
				}
				//批量删除数据
				var idstrs=ids.join(",");
				$http({
					method:'POST',
					url:'../mall/member/membergrade/del.action',
					data:{
						'ids':idstrs
					}
				}).
				success(function(data,status,headers,config){
					if(data.sucflag){
						$scope.info={
								show:true,
								msg:'批量删除成功'
						}
						//window.location.reload();
					}
					
				}).
				error(function(data,status,headers,config){
					$scope.errors={
							show:true,
							msg:'系统异常'
					}
				});
			}
		}

		/**
		 * 列表全选
		 */
		function toggleAll(selectAll,selectedItems){
			for(var id in selectedItems){
				if(selectedItems.hasOwnProperty(id)){
					selectedItems[id]=selectAll;
				}
			}
		}
		/**
		 * 列表单选
		 */
		function toggleOne(selectedItems){
			var me=this;
			for(var id in selectedItems){
				if(selectedItems.hasOwnProperty(id)){
					if(!selectedItems[id]){
						me.selectAll=false;
					}
				}
			}
			me.selectAll=true;
		}

		function someClickHandler(info) {
	        vm.message = info.id + ' - ' + info.name;
	    }
		/**
		 * 单击列表某行回调
		 */
		function rowCallback(nRow,aData,iDisplayIndex,iDisplayIndexFull){
			$('td', nRow).unbind('click');
	        $('td', nRow).bind('click', function() {
	            $scope.$apply(function() {
	                vm.someClickHandler(aData);
	            });
	        });
	        return nRow;
		}
	}
    /*=====End Of Find List MemberGradeT=====*/

});